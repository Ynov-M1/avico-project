using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WebApiJwt.Models;

namespace WebApiJwt.Entities
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public DbSet<HotelModel> Hotel {get; set;}
        public DbSet<ChambreModel> Chambre {get; set;}
        public DbSet<TableModel> Chambre {get; set;}
        public DbSet<ReservationModel> Chambre {get; set;}
        public DbSet<RestaurantModel> Chambre {get; set;}
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySql(GetConnectionString());
        }

        private static string GetConnectionString()
        {
            const string databaseName = "webapijwt";
            const string databaseUser = "root";
            const string databasePass = "root";

            return $"Server=localhost;" +
                   $"port = 8889;" +
                   $"database={databaseName};" +
                   $"uid={databaseUser};" +
                   $"pwd={databasePass};" +
                   $"pooling=true;";
        }
    }
}