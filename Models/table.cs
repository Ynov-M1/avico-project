﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace WebApiJwt.Models
{
    public class TableModel
    {
        public int ID { get; set; }

        [Required]
        [Display(DateTime = "created_at")]
        public DateTime created_at { get; set; }

        [Required]
        [Key]
        public string uniqid { get; set; }

        [Required]
        [Display(NbPlaces = "NbPlaces")]
        public int nbplaces { get; set; }
        
        [Display(Number = "Number")]
        public string number { get; set; }
        
    }

}