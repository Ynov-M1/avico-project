﻿using System;

public class chambre
{
    public int ID { get; set; }

    [Required]
    [Display(DateTime = "created_at")]
    public DateTime created_at { get; set; }

    [Required]
    [Key]
    public string uniqid { get; set; }

    [Required]
    [Display(name = "Name")]
    public string name { get; set; }

    [Required]
    [Display(image = "Image")]
    public string image { get; set; }

    [Required]
    [Display(taille = "TailleChambre")]
    public int taille { get; set; }

    [Required]
    [Display(nbPersonne = "NbPersonne")]
    public int nbPersonne { get; set; }

    [Required]
    [Display(nbLit = "NbLit")]
    public int nbLit { get; set; }

    [Required]
    [Display(description = "Description")]
    public string description { get; set; }

    public hotels lHotel { get; set; }
}
