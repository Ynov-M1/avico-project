using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace WebApiJwt.Models{
    public class HotelModel
    {
        public int ID { get; set; }

        [Required]
        [Display(DateTime = "created_at")]
        public DateTime created_at { get; set; }

        [Required]
        [Key]
        public string uniqid { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string name { get; set; }

        [Required]
        [Display(description = "Description")]
        public string description { get; set; }

        [Required]
        [Display(image = "image")]
        public string image { get; set; }
 
        public List<chambre> lesChambres {get; set;}
    }

}