﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace WebApiJwt.Models
{
    public class ReservationModel
    {
        public int ID { get; set; }

        [Required]
        [Display(DateTime = "created_at")]
        public DateTime created_at { get; set; }

        [Required]
        [Key]
        public string uniqid { get; set; }

        [Required]
        [Display(NbPersonnes = "NbPersonnes")]
        public string nbpersonnes { get; set; }

        [Required]
        [Display(Date = "Date")]
        public DateTime date { get; set; }

        [Display(Commentary = "Commentary")]
        public string commentary { get; set; }
    }

}