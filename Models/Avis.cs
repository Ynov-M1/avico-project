﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace WebApiJwt.Models
{
    public class AvisModel
    {
        public int ID { get; set; }

        [Required]
        [Display(DateTime = "created_at")]
        public DateTime created_at { get; set; }

        [Required]
        [Key]
        public string uniqid { get; set; }

        [Required]
        [Display(Review = "Review")]
        public string review { get; set; }

        [Required]
        [Display(Date = "Date")]
        public DateTime date { get; set; }

        [Required]
        [Display(Note = "Note")]
        public string note { get; set; }

    }

}