﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace WebApiJwt.Models
{
    public class RestaurantModel
    {
        public int ID { get; set; }

        [Required]
        [Display(DateTime = "created_at")]
        public DateTime created_at { get; set; }

        [Required]
        [Key]
        public string uniqid { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string name { get; set; }

        [Required]
        [Display(Description = "Description")]
        public string description { get; set; }

        [Display(Image = "Image")]
        public string image { get; set; }

        [Required]
        [Display(Location = "Location")]
        public string location { get; set; }
        
        [Display(Phone = "Phone")]
        public string phone { get; set; }
        
        [Display(Mail = "Mail")]
        public string mail { get; set; }

    }

}